package saucedemo_webuitesting.uiview.modals;

import core.base_action.WebAction;
import core.base_ui.BaseWebUI;

public class UIModalDialog<M,V> extends BaseWebUI<M,V> {

    public UIModalDialog(M m, V v, WebAction action)
    {
        super(m,v, action);
    }

}

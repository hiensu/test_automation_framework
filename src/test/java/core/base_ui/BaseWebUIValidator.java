package core.base_ui;

import core.base_action.SoftAssertExt;

public class BaseWebUIValidator<M>  {
    private M map;
    protected SoftAssertExt softAssert;
    public BaseWebUIValidator() {
    }

    public M Map() {
        return map;
    }
    public void setMap(M m) {
        map = m;
    }

    public SoftAssertExt getSoftAssert() {
        return softAssert;
    }

    public void setSoftAssert(SoftAssertExt sAssert) {
        softAssert = sAssert;
    }
}
